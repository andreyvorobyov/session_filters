
import json
import uuid
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.core import serializers 
from .models import Message


def get_session_data(session):
    user_id=session.get('user_id', None)
    if not user_id:
        session['user_id'] = str(uuid.uuid1())
    session.modified = True
    return {'user_id': session['user_id']}

def home(request):
    session_data = get_session_data(request.session)
    messages = Message.filters.user_id(session_data['user_id'])
    return render(request, 'home.html', {'messages': messages})


@csrf_exempt
def add_message(request):
    session_data=get_session_data(request.session)
    #write POST data in to the DataBase
    if request.method == 'POST':
        title=''
        comment=''
        json_data = json.loads(request.body)
        try:
            title = json_data['title']
        except KeyError:
            pass
        try:
            comment = json_data['comment']
        except KeyError:
            pass

        if title and comment:
            message = Message()
            message.title = title
            message.comment = comment
            message.user_id = session_data['user_id']
            message.save()

    #return JSON data to Client
    message_list = Message.filters.user_id(session_data['user_id'])
    json_text = serializers.serialize('json', message_list)
    response = HttpResponse()
    response['Content-Type'] = 'text/javascript'
    response.write(json_text)
    return response