from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^$', views.home), 
    url(r'add-message/$', views.add_message),
] 
