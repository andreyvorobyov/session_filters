from __future__ import unicode_literals

from django.db import models
from django.contrib import admin
import datetime

class FilterManager(models.Manager):
    def user_id(self, id):
        return super(FilterManager, self).get_queryset().filter(user_id=id)

class Message(models.Model):
    title = models.CharField(max_length=50)
    comment = models.TextField()
    created = models.DateTimeField(default = datetime.datetime.now)
    user_id = models.CharField(max_length=100)

    filters = FilterManager()

class MessageAdmin(admin.ModelAdmin):
    list_display = ('title', 'created')
   
   
admin.site.register(Message, MessageAdmin)


